package au.com.test.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;

import junit.framework.TestCase;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import au.com.test.service.CreditCardService;

/**
 * The class <code>TransactionTest</code> contains tests for the class {@link <code>Transaction</code>}
 *
 * @pattern JUnit Test Case
 *
 * @generatedBy CodePro at 21/05/14 5:18 PM
 *
 * @author wallaceau
 *
 * @version $Revision$
 */
public class TransactionTest {

    /**
     * Perform pre-test initialization
     *
     * @throws Exception
     *
     * @see TestCase#setUp()
     */
    //    @Before
    //    public void setUp() throws Exception {
    //        // Add additional set up code here
    //    }

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testBuildTransactionExpectIllegalArgumentException() {
        // wrong input
        Transaction.buildTransaction("sfsafd,2014-05-04T11:11:11");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuildTransactionExpectIllegalArgumentException2() {
        // wrong data format
        Transaction.buildTransaction("sfsafd,2014-AA-04T11:11:11,11");
    }

    @Test
    public void testBuildTransactionExpectCreateNewInstance() {
        assertNotNull(Transaction.buildTransaction("sfsafd,2014-05-04T11:11:11,11"));
    }

    @Test
    public void testBuildTransactionExpectSetCreditCardNumber() {
        String creditCardNumber = "dsfdfw342r2324234";
        Transaction tran = new Transaction();
        
        Method method;
        try {
            method = Transaction.class.getDeclaredMethod("setCreditCardHashNum", String.class);
            method.setAccessible(true);
            method.invoke(tran, creditCardNumber);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        
        assertEquals(creditCardNumber, tran.getCreditCardHashNum());
    }
    @Test
    public void testBuildTransactionExpectSetPrice() {
        String price = "11.11";
        Transaction tran = new Transaction();
        
        Method method;
        try {
            method = Transaction.class.getDeclaredMethod("setPrice", String.class);
            method.setAccessible(true);
            method.invoke(tran, price);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        
        assertEquals(11.11, tran.getPrice(),2);
    }
    @Test
    public void testBuildTransactionExpectSetTimestamp() throws ParseException {
        String timestampStr = "2014-12-12T01:11:11";
        Transaction tran = new Transaction();
        Method method;
        try {
            method = Transaction.class.getDeclaredMethod("setTimestamp", String.class);
            method.setAccessible(true);
            method.invoke(tran, timestampStr);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");
        DateTime dt = formatter.parseDateTime(timestampStr);
        assertEquals(dt.toDate().getTime(), tran.getTimestamp().getTime());
    }
}

