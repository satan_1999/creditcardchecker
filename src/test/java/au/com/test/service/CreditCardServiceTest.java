/**
 * 
 */
package au.com.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import au.com.test.domain.Transaction;

/**
 * @author wallaceau
 *
 */
public class CreditCardServiceTest {
    private String filePath;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        String path = getClass().getClassLoader().getResource(".").getPath();
        filePath = path + "resource/data.csv";
    }

    private CreditCardService mockCreditCardService() {
        CreditCardService fixture = new CreditCardService();
        List<Transaction> list = new ArrayList<Transaction>();
        list.add(Transaction.buildTransaction("3df434534dsfsdfsdf32423, 2014-05-21T21:11:22,  1100.01"));
        list.add(Transaction.buildTransaction("3df434534dsfsdfsdf32423, 2014-05-21T21:11:22,  1100.01"));
        list.add(Transaction.buildTransaction("sdf434534dsfsdfsdf32423, 2014-05-21T11:11:22,  11.01"));
        list.add(Transaction.buildTransaction("44df434534dsfsdfsdf32423, 2014-05-22T11:11:22,  11.01"));
        Field field;
        try {
            field = CreditCardService.class.getDeclaredField("transactions");
            field.setAccessible(true);
            field.set(fixture, list);
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return fixture;
    }


    @Test
    public void testReadTransactionFromFileExpectloadSomeTransactions() throws Exception {
        CreditCardService fixture = new CreditCardService();
        List<Transaction> result = fixture.readTransactionFromFile(filePath);
        // add additional test code here
        assertNotNull(result);
        assertEquals(4, result.size());
    }
    
    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testReadTransactionFromFileExpectIllegalArgumentException() throws Exception {
        CreditCardService fixture = new CreditCardService();
        fixture.readTransactionFromFile(null);
    }
    
    @Test
    public void testReadTransactionFromFileExpectLoadNoTransaction() throws Exception {
        CreditCardService fixture = new CreditCardService();
        List<Transaction> result = fixture.readTransactionFromFile("");
        assertNotNull(result);
        assertEquals(0, result.size());
    }


    @Test
    public void testTotalAmountPerCraditCardByDayExpect2EntryInReturnedMap() {
        CreditCardService fixture = mockCreditCardService();
        Map result = fixture.totalAmountPerCraditCardByDay(new DateTime(2014, 5, 21, 2, 2, 2).toDate());
        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void testCheckFraudulentsByDateExpectReturnedListHasOneElement() {
        CreditCardService fixture = new CreditCardService();
        CreditCardService spy = Mockito.spy(fixture);
        Map<String, Double> totalAmountPerCraditCardsByDay = new HashMap<String, Double>();
        totalAmountPerCraditCardsByDay.put("12aaa", 123.01);
        totalAmountPerCraditCardsByDay.put("12aab", 103.01);
        totalAmountPerCraditCardsByDay.put("122ab", 11.01);
        Mockito.doReturn(totalAmountPerCraditCardsByDay).when(spy).totalAmountPerCraditCardByDay(Mockito.any(Date.class));
        List<String> result = spy.checkFraudulentsByDate(110.00, new DateTime(2014, 5, 21, 2, 2, 2).toDate());
        assertEquals(1, result.size());
        assertEquals("12aaa", result.get(0));
    }
    
    @Test
    public void testInitEpxectCalledReadTransactionFromFile() {
        CreditCardService spy = Mockito.spy(mockCreditCardService());
        List<Transaction> transactions = new ArrayList();
        Mockito.doReturn(transactions).when(spy).readTransactionFromFile(Mockito.any(String.class));
        spy.init();
        assertEquals(0, spy.getTransactions().size());
    }

}
