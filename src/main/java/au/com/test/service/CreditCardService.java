package au.com.test.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import au.com.test.domain.Transaction;

@Service
public class CreditCardService {

    protected static Logger LOGGER = Logger.getLogger(CreditCardService.class);
    private String batchfileLocation;
    private List<Transaction> transactions;

    /*
     * Load transactions information from file
     */
    public void init() {
        LOGGER.debug("Calling init");
        LOGGER.setLevel(Level.DEBUG);
        String path = getClass().getClassLoader().getResource(".").getPath();
        batchfileLocation = path + "resource/data.csv";
        // load transactions information from file
        transactions = readTransactionFromFile(batchfileLocation);
    }

    /**
     * Check Fraudulency by Date and Transaction Threshold
     */
    public List<String> checkFraudulentsByDate(double transactionThreshold, Date transactionDate) {
        LOGGER.debug("Calling checkFraudulentsByDate");
        List<String> fraudulents = new ArrayList<String>();
        // CreditCard Number, total amount of price
        Map<String, Double> totalAmountPerCraditCardsByDay = totalAmountPerCraditCardByDay(transactionDate);
        if(totalAmountPerCraditCardsByDay.isEmpty()){
            return fraudulents;
        }
        Iterator<Entry<String, Double>> it = totalAmountPerCraditCardsByDay.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Double> totalAmountPerCraditCard = it.next();
            if(totalAmountPerCraditCard.getValue() > transactionThreshold){
                // find the amount greater than transactionThreshold
                fraudulents.add(totalAmountPerCraditCard.getKey());
            }
        }
      
        return fraudulents;
    }

    /**
     * Read Transaction information from file and create Transaction domain class
     * 
     * @param fileName
     * @return
     * @throws IOException
     */
    public List<Transaction> readTransactionFromFile(String fileName) {
        Assert.notNull(fileName, "The fileName must not be null");
        List<Transaction> transactionList = new ArrayList<Transaction>();
        BufferedReader in = null;

        try {
            File file = new File(fileName);
            in = new BufferedReader(new FileReader(file));

            String line;
            while ((line = in.readLine()) != null) {
                if(line.isEmpty()){
                    continue;
                }
                // populate Transaction domain
                try {
                    transactionList.add(Transaction.buildTransaction(line));
                } catch (Exception ex) {
                    LOGGER.error("Failed to build one trandaction, line: " + line, ex);
                }
            }
            LOGGER.info("Loaded Employees  " + transactionList.size());
        } catch (IOException e) {
            LOGGER.error("Failed to load file", e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    LOGGER.error("Failed to close file", e);
                }
            }
        }
        return transactionList;
    }

    public Map<String, Double> totalAmountPerCraditCardByDay(Date transactionDate) {
        Map<String, Double> totalAmoutPerCraditCard = new HashMap<String, Double>();
        DateTime startOfDay = new DateTime(transactionDate.getTime()).millisOfDay().withMinimumValue();
        DateTime endOfDay = new DateTime(transactionDate.getTime()).millisOfDay().withMaximumValue();

        LOGGER.debug("startOfDay: " + startOfDay);
        LOGGER.debug("endOfDay: " + endOfDay);

        Interval interval = new Interval(startOfDay, endOfDay);

        for (Transaction transaction : getTransactions()) {
            if (interval.contains(transaction.getTimestamp().getTime())) {
                // in the specific date
                if (!totalAmoutPerCraditCard.containsKey(transaction.getCreditCardHashNum())) {
                    // find new creditcard number
                    totalAmoutPerCraditCard.put(transaction.getCreditCardHashNum(), transaction.getPrice());
                } else {
                    // find existing creditcard number : add the new price into existing price
                    totalAmoutPerCraditCard.put(transaction.getCreditCardHashNum(),
                            totalAmoutPerCraditCard.get(transaction.getCreditCardHashNum()) + transaction.getPrice());
                }
            }

        }
        return totalAmoutPerCraditCard;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

}
