package au.com.test.domain;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

public class Transaction {
    private static Logger log = Logger.getLogger(Transaction.class);
    private static final char LINE_SEPERATOR = ',';
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private String creditCardHashNum;
    private Date timestamp;
    private double price;
    
    /*
     * Builder of the transaction domain
     */
    public static Transaction buildTransaction(String transactionStr) {
        String[] splitLine = StringUtils.split(transactionStr, LINE_SEPERATOR);
        Assert.isTrue(splitLine.length == 3, "This transaction is not correct :"+transactionStr);
        Transaction tran = new Transaction();
        try {
            tran.setCreditCardHashNum(splitLine[0].trim()).setTimestamp(splitLine[1].trim()).setPrice(splitLine[2].trim());
            return tran;
        } catch (Exception ex) {
            log.error("Failed to build transaction", ex);
            throw new IllegalArgumentException("This transaction is not correct");
        }
    }

    private Transaction setCreditCardHashNum(String creditCardHashNum) {
        this.creditCardHashNum = creditCardHashNum;
        return this;
    }

    private Transaction setTimestamp(String timestampStr) throws ParseException {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        this.timestamp = df.parse(timestampStr);
        return this;
    }

    private Transaction setPrice(String priceStr) {
        this.price = Double.parseDouble(priceStr);
        return this;
    }

    public String getCreditCardHashNum() {
        return creditCardHashNum;
    }


    public Date getTimestamp() {
        return timestamp;
    }


    public double getPrice() {
        return price;
    }

}
