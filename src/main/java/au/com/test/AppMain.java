package au.com.test;

import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.joda.time.DateTime;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import au.com.test.service.CreditCardService;

/**
 * Hello world!
 *
 */
public class AppMain {
    static CreditCardService service;

    /**
     * Start to run the program
     * 
     * @param args
     */
    public static void main(final String[] args) {
        BasicConfigurator.configure();
        // Create Service CreditCardService
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "resource/applicationContext.xml" });
        service = (CreditCardService) context.getBean("creditCardService");
        // load transactions from file, init() can be called when spring creates the bean
        service.init();
        // print out the fraudulents

        printOutFraudulents(120.01, new DateTime(2014, 5, 20, 0, 1, 0));
        printOutFraudulents(100.01, new DateTime(2014, 5, 21, 0, 1, 0));
    }

    public static void printOutFraudulents(double transactionThreshold, DateTime transactionDate) {
        List<String> fraudulents = service.checkFraudulentsByDate(120.01, transactionDate.toDate());
        // print out the fraudulents
        System.out.println("Fraudulency for transactionThreshold(" + transactionThreshold + ") transactionDate("
                + transactionDate.toString("yyyy-MM-dd") + ")");
        for (String fraudulent : fraudulents) {
            System.out.println(fraudulent);
        }
    }
}
